package de.cube.hotel.employees.functions;

import de.cube.hotel.employees.classes.Chef;
import de.cube.hotel.employees.classes.Manager;
import de.cube.hotel.employees.classes.Washer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Utilities {

    static int getRowOneColumnOne(ResultSet rs) {
        try {
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    static void updateChef(Chef chef, Connection connection) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE chef SET age=?, firstName=?, lastName=?, email=?, earningsPerHour=? WHERE id=?;");
            statement.setInt(1, chef.getAge());
            statement.setString(2, chef.getFirstName());
            statement.setString(3, chef.getLastName());
            statement.setString(4, chef.getEmail());
            statement.setDouble(5, chef.getEarningsPerHour());
            statement.setInt(6, chef.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    static void updateManager(Manager manager, Connection connection) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE manager SET age=?, firstName=?, lastName=?, email=?, earningsPerHour=? WHERE id=?;");
            statement.setInt(1, manager.getAge());
            statement.setString(2, manager.getFirstName());
            statement.setString(3, manager.getLastName());
            statement.setString(4, manager.getEmail());
            statement.setDouble(5, manager.getEarningsPerHour());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    static void updateWasher(Washer washer, Connection connection) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE washer SET age=?, firstName=?, lastName=?, email=?, earningsPerHour=? WHERE id=?;");
            statement.setInt(1, washer.getAge());
            statement.setString(2, washer.getFirstName());
            statement.setString(3, washer.getLastName());
            statement.setString(4, washer.getEmail());
            statement.setDouble(5, washer.getEarningsPerHour());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
