package de.cube.hotel.employees.classes;

public class BaseEmployee {

    private int id;

    private int age;

    private String firstName;
    private String lastName;

    private String email;

    private double earningsPerHour;

    private boolean hasChanged = false;

    public BaseEmployee(int id, int age, String firstName, String lastName, String email, double earningsPerHour) {
        this.id = id;
        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.earningsPerHour = earningsPerHour;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        hasChanged = true;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        hasChanged = true;
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        hasChanged = true;
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        hasChanged = true;
        this.email = email;
    }

    public boolean hasChanged(){return hasChanged;}

    public double getEarningsPerHour() {
        return earningsPerHour;
    }

    public void setEarningsPerHour(double earningsPerHour) {
        this.earningsPerHour = earningsPerHour;
        hasChanged = true;
    }
}
