package de.cube.hotel.employees.classes;

public class Chef extends BaseEmployee {
    public Chef(int id, int age, String firstName, String lastName, String email) {
        super(id, age, firstName, lastName, email, 10);
    }
}
