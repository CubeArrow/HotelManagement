package de.cube.hotel.rooms.classes;

import java.util.Date;

public class Suite extends BaseRoom {
    public Suite(int id, int roomNumber, int isUsed, java.sql.Date lastUsedDay) {
        super(id, roomNumber, isUsed, lastUsedDay, 200);
    }
}
