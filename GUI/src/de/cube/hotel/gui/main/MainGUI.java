package de.cube.hotel.gui.main;

import de.cube.hotel.gui.scenes.Customers;
import de.cube.hotel.gui.scenes.Employees;
import de.cube.hotel.gui.scenes.Rooms;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

public class MainGUI extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {

        TabPane tabPane = new TabPane();
        Customers customers = new Customers();
        Tab customersTab = new Tab("Customers", customers.create());
        Employees employees = new Employees();
        Tab employeesTab = new Tab("Employees", employees.create());
        Rooms rooms = new Rooms();
        Tab roomsTab = new Tab("Rooms", rooms.create());

        primaryStage.setOnCloseRequest(event -> {
            customers.manageCustumers.save();
            employees.manageEmployees.save();
            rooms.manageRooms.save();

            System.exit(0);
        });
        tabPane.getTabs().addAll(employeesTab, roomsTab, customersTab);
        primaryStage.setScene(new Scene(tabPane));
        primaryStage.show();
    }
}
