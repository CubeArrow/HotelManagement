package de.cube.hotel.gui.scenes;

import de.cube.hotel.rooms.classes.DoubleRoom;
import de.cube.hotel.rooms.classes.SingleRoom;
import de.cube.hotel.rooms.classes.Suite;
import de.cube.hotel.rooms.functions.ManageRooms;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.util.converter.BooleanStringConverter;
import javafx.util.converter.IntegerStringConverter;

import javax.swing.*;
import java.sql.Date;

public class Rooms {
    public ManageRooms manageRooms = new ManageRooms();

    public TabPane create() {
        TabPane tabPane = new TabPane();

        Tab singleRoomTab = new Tab("Single Room");
        BorderPane singlePane = new BorderPane();
        Button new_single_room = new Button("New Single Room");
        new_single_room.setOnAction(event -> {
            int roomNumber = Integer.parseInt(JOptionPane.showInputDialog("What is the Room number?"));
            int isUsed = JOptionPane.showConfirmDialog(null, "Is the room used?");
            java.sql.Date date = Date.valueOf(JOptionPane.showInputDialog("What is the last used day?"));

            manageRooms.addSingleRoom(roomNumber, isUsed, date);
            singlePane.setCenter(singleRoomTableView());
        });

        Button remove_single_room = new Button("Remove Single Room");
        remove_single_room.setOnAction(event -> {
            manageRooms.removeSingleRoom(Integer.parseInt(JOptionPane.showInputDialog("What is the id?")));
            singlePane.setCenter(singleRoomTableView());
        });
        HBox hBox1 = new HBox();
        hBox1.getChildren().addAll(new_single_room, remove_single_room);
        singlePane.setTop(hBox1);
        singlePane.setCenter(singleRoomTableView());
        singleRoomTab.setContent(singlePane);
        tabPane.getTabs().add(singleRoomTab);


        Tab doubleRoomTab = new Tab("Double Rooms");
        BorderPane doubleRoomPane = new BorderPane();

        Button new_double_room = new Button("New Double Room");
        new_double_room.setOnAction(event -> {
            int roomNumber = Integer.parseInt(JOptionPane.showInputDialog("What is the Room number?"));
            int isUsed = JOptionPane.showConfirmDialog(null, "Is the room used?");
            java.sql.Date date = Date.valueOf(JOptionPane.showInputDialog("What is the last used day?"));

            manageRooms.addDoubleRoom(roomNumber, isUsed, date);
            doubleRoomPane.setCenter(doubleRoomTableView());
        });

        Button remove_double_room = new Button("Remove Double Room");
        remove_double_room.setOnAction(event -> {
            manageRooms.removeDoubleRoom(Integer.parseInt(JOptionPane.showInputDialog("What is the id?")));
            doubleRoomPane.setCenter(doubleRoomTableView());
        });
        HBox hBox = new HBox();
        hBox.getChildren().addAll(new_double_room, remove_double_room);
        doubleRoomPane.setTop(hBox);

        doubleRoomPane.setCenter(doubleRoomTableView());
        doubleRoomTab.setContent(doubleRoomPane);
        tabPane.getTabs().add(doubleRoomTab);

        Tab suiteTab = new Tab("Suites");
        BorderPane suitePane = new BorderPane();

        Button new_suite = new Button("New Suite");
        new_suite.setOnAction(event -> {
            int roomNumber = Integer.parseInt(JOptionPane.showInputDialog("What is the Room number?"));
            int isUsed = JOptionPane.showConfirmDialog(null, "Is the room used?");
            java.sql.Date date = Date.valueOf(JOptionPane.showInputDialog("What is the last used day?"));

            manageRooms.addSuite(roomNumber, isUsed, date);
            suitePane.setBottom(suiteTableView());
        });

        Button remove_suite = new Button("Remove Suite");
        remove_suite.setOnAction(event -> {
            manageRooms.removeSuite(Integer.parseInt(JOptionPane.showInputDialog("What is the id?")));
            suitePane.setBottom(suiteTableView());
        });
        suitePane.setTop(new HBox(new_suite, remove_suite));

        suitePane.setCenter(suiteTableView());
        suiteTab.setContent(suitePane);
        tabPane.getTabs().add(suiteTab);

        return tabPane;
    }

    private TableView singleRoomTableView() {
        TableView<SingleRoom> customerTableView = new TableView<>();
        customerTableView.setEditable(true);


        TableColumn<SingleRoom, Integer> id = new TableColumn<>("id");
        id.setEditable(false);

        TableColumn<SingleRoom, Integer> roomNumber = new TableColumn<>("Room number");
        roomNumber.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        roomNumber.setOnEditCommit(event -> event.getRowValue().setRoomNumber(event.getNewValue()));

        TableColumn<SingleRoom, Boolean> isUsed = new TableColumn<>("is Used?");
        isUsed.setCellFactory(TextFieldTableCell.forTableColumn(new BooleanStringConverter()));
        isUsed.setOnEditCommit(event -> {
            SingleRoom rowValue = event.getRowValue();
            if (event.getNewValue())
                rowValue.setUsed(1);
            else
                rowValue.setUsed(0);
        });


        TableColumn<SingleRoom, String> last_used_day = new TableColumn<>("Last used Day");
        last_used_day.setCellFactory(TextFieldTableCell.forTableColumn());
        last_used_day.setOnEditCommit(event -> event.getRowValue().setLastUsedDay(Date.valueOf(event.getNewValue())));


        roomNumber.setCellValueFactory(new PropertyValueFactory<>("roomNumber"));
        last_used_day.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getLastUsedDay().toString()));
        isUsed.setCellValueFactory(c -> new SimpleBooleanProperty(c.getValue().isUsedBool()));
        id.setCellValueFactory(new PropertyValueFactory<>("id"));


        ObservableList<SingleRoom> list = FXCollections.observableArrayList();
        list.addAll(manageRooms.getSingleRooms());

        customerTableView.getColumns().addAll(id, roomNumber, isUsed, last_used_day);
        customerTableView.setItems(list);
        return customerTableView;
    }

    private TableView doubleRoomTableView() {
        TableView<DoubleRoom> customerTableView = new TableView<>();
        customerTableView.setEditable(true);


        TableColumn<DoubleRoom, Integer> id = new TableColumn<>("id");
        id.setEditable(false);

        TableColumn<DoubleRoom, Integer> roomNumber = new TableColumn<>("Room number");
        roomNumber.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        roomNumber.setOnEditCommit(event -> event.getRowValue().setRoomNumber(event.getNewValue()));

        TableColumn<DoubleRoom, Boolean> isUsed = new TableColumn<>("is Used?");
        isUsed.setCellFactory(TextFieldTableCell.forTableColumn(new BooleanStringConverter()));
        isUsed.setOnEditCommit(event -> {
            DoubleRoom rowValue = event.getRowValue();
            if (event.getNewValue())
                rowValue.setUsed(1);
            else
                rowValue.setUsed(0);
        });


        TableColumn<DoubleRoom, String> last_used_day = new TableColumn<>("Last used Day");
        last_used_day.setCellFactory(TextFieldTableCell.forTableColumn());
        last_used_day.setOnEditCommit(event -> event.getRowValue().setLastUsedDay(Date.valueOf(event.getNewValue())));


        roomNumber.setCellValueFactory(new PropertyValueFactory<>("roomNumber"));
        last_used_day.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getLastUsedDay().toString()));
        isUsed.setCellValueFactory(c -> new SimpleBooleanProperty(c.getValue().isUsedBool()));
        id.setCellValueFactory(new PropertyValueFactory<>("id"));


        ObservableList<DoubleRoom> list = FXCollections.observableArrayList();
        list.addAll(manageRooms.getDoubleRooms());

        customerTableView.getColumns().addAll(id, roomNumber, isUsed, last_used_day);
        customerTableView.setItems(list);
        return customerTableView;
    }

    private TableView suiteTableView() {
        TableView<Suite> customerTableView = new TableView<>();
        customerTableView.setEditable(true);


        TableColumn<Suite, Integer> id = new TableColumn<>("id");
        id.setEditable(false);

        TableColumn<Suite, Integer> roomNumber = new TableColumn<>("Room number");
        roomNumber.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        roomNumber.setOnEditCommit(event -> event.getRowValue().setRoomNumber(event.getNewValue()));

        TableColumn<Suite, Boolean> isUsed = new TableColumn<>("is Used?");
        isUsed.setCellFactory(TextFieldTableCell.forTableColumn(new BooleanStringConverter()));
        isUsed.setOnEditCommit(event -> {
            Suite rowValue = event.getRowValue();
            if (event.getNewValue())
                rowValue.setUsed(1);
            else
                rowValue.setUsed(0);
        });

        TableColumn<Suite, Integer> costPerDay = new TableColumn<>("Cost per day");
        costPerDay.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        costPerDay.setOnEditCommit(event -> event.getRowValue().setCostPerDay(event.getNewValue()));

        TableColumn<Suite, String> last_used_day = new TableColumn<>("Last used Day");
        last_used_day.setCellFactory(TextFieldTableCell.forTableColumn());
        last_used_day.setOnEditCommit(event -> event.getRowValue().setLastUsedDay(Date.valueOf(event.getNewValue())));

        costPerDay.setCellValueFactory(new PropertyValueFactory<>("costPerDay"));
        roomNumber.setCellValueFactory(new PropertyValueFactory<>("roomNumber"));
        last_used_day.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getLastUsedDay().toString()));
        isUsed.setCellValueFactory(c -> new SimpleBooleanProperty(c.getValue().isUsedBool()));
        id.setCellValueFactory(new PropertyValueFactory<>("id"));


        ObservableList<Suite> list = FXCollections.observableArrayList();
        list.addAll(manageRooms.getSuites());

        customerTableView.getColumns().addAll(id, roomNumber, isUsed, last_used_day);
        customerTableView.setItems(list);
        return customerTableView;
    }

}
