package de.cube.hotel.gui.scenes;

import de.cube.hotel.customers.classes.NormalCustomer;
import de.cube.hotel.customers.classes.VIP;
import de.cube.hotel.customers.functions.ManageCustumers;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.util.converter.IntegerStringConverter;

import javax.swing.*;

public class Customers {
    public ManageCustumers manageCustumers = new ManageCustumers();

    public TabPane create(){
        TabPane tabPane = new TabPane();

        Tab customersTab = new Tab("Normal Customers");
        BorderPane pane = new BorderPane();
        Button new_normal_customer = new Button("New Normal Customer");
        new_normal_customer.setOnAction(event -> {
            String firstName = JOptionPane.showInputDialog("What is the First name?");
            String lastName = JOptionPane.showInputDialog("What is the Last name?");
            int age = Integer.parseInt(JOptionPane.showInputDialog("What is his/her age?"));
            String email = JOptionPane.showInputDialog("What is his/her email address?");
            int roomId = Integer.parseInt(JOptionPane.showInputDialog("What is his/her room id?"));

            manageCustumers.addNomalCustomer(age, firstName, lastName, email, roomId);
            pane.setCenter(normalTableView());
        });
        Button remove_normal_customer = new Button("Remove Normal Customer");
        remove_normal_customer.setOnAction(event -> {
            manageCustumers.removeNormalCustomer(Integer.parseInt(JOptionPane.showInputDialog("What is the id?")));
            pane.setCenter(normalTableView());
        });
        HBox box = new HBox();
        box.getChildren().addAll(new_normal_customer, remove_normal_customer);
        pane.setTop(box);
        pane.setCenter(normalTableView());
        customersTab.setContent(pane);
        tabPane.getTabs().add(customersTab);

        Tab vipTab = new Tab("VIP Customers");
        BorderPane borderPane = new BorderPane();

        Button new_vip_customer = new Button("New VIP Customer");
        new_vip_customer.setOnAction(event -> {
            String firstName = JOptionPane.showInputDialog("What is the First name?");
            String lastName = JOptionPane.showInputDialog("What is the Last name?");
            int age = Integer.parseInt(JOptionPane.showInputDialog("What is his/her age?"));
            String email = JOptionPane.showInputDialog("What is his/her email address?");
            int roomId = Integer.parseInt(JOptionPane.showInputDialog("What is his/her room id?"));

            manageCustumers.addVIP(age, firstName, lastName, email, roomId);
            pane.setCenter(vipTableView());
        });
        Button remove_vip = new Button("Remove VIP");
        remove_vip.setOnAction(event -> {
            manageCustumers.removeVIP(Integer.parseInt(JOptionPane.showInputDialog("What is the id?")));
            borderPane.setCenter(vipTableView());
        });
        HBox hBox = new HBox();
        hBox.getChildren().addAll(new_vip_customer, remove_vip);
        borderPane.setTop(hBox);

        borderPane.setCenter(vipTableView());
        vipTab.setContent(borderPane);
        tabPane.getTabs().add(vipTab);

        return tabPane;
    }

    private TableView normalTableView(){
        TableView<NormalCustomer> customerTableView = new TableView<>();

        TableColumn<NormalCustomer, Integer> id = new TableColumn<>("id");
        id.setEditable(false);


        TableColumn<NormalCustomer, String> fullName = new TableColumn<>("Full Name");

        TableColumn<NormalCustomer, String> firstName = new TableColumn<>("First Name");
        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        fullName.setOnEditCommit(event -> event.getRowValue().setFirstName(event.getNewValue()));

        TableColumn<NormalCustomer, String> lastName = new TableColumn<>("Last Name");
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> event.getRowValue().setLastName(event.getNewValue()));

        TableColumn<NormalCustomer, Integer> age = new TableColumn<>("age");
        age.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        age.setOnEditCommit(event -> event.getRowValue().setAge(event.getNewValue()));

        TableColumn<NormalCustomer, String> email = new TableColumn<>("Email");
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> event.getRowValue().setEmail(event.getNewValue()));

        TableColumn<NormalCustomer, Integer> roomId = new TableColumn<>("Room Id");
        roomId.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        roomId.setOnEditCommit(event -> event.getRowValue().setRoomId(event.getNewValue()));

        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        age.setCellValueFactory(new PropertyValueFactory<>("age"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        roomId.setCellValueFactory(new PropertyValueFactory<>("roomId"));


        fullName.getColumns().addAll(firstName, lastName);

        ObservableList<NormalCustomer> list = FXCollections.observableArrayList();
        list.addAll(manageCustumers.getNormalCustomers());

        customerTableView.getColumns().addAll(id, fullName, age, email);
        customerTableView.setItems(list);
        return customerTableView;
    }

    private TableView vipTableView(){
        TableView<VIP> customerTableView = new TableView<>();

        TableColumn<VIP, Integer> id = new TableColumn<>("id");
        id.setEditable(false);

        TableColumn<VIP, String> fullName = new TableColumn<>("Full Name");

        TableColumn<VIP, String> firstName = new TableColumn<>("First Name");
        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        fullName.setOnEditCommit(event -> event.getRowValue().setFirstName(event.getNewValue()));

        TableColumn<VIP, String> lastName = new TableColumn<>("Last Name");
        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setOnEditCommit(event -> event.getRowValue().setLastName(event.getNewValue()));

        TableColumn<VIP, Integer> age = new TableColumn<>("age");
        age.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        age.setOnEditCommit(event -> event.getRowValue().setAge(event.getNewValue()));

        TableColumn<VIP, String> email = new TableColumn<>("Email");
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit(event -> event.getRowValue().setEmail(event.getNewValue()));

        TableColumn<VIP, Integer> roomId = new TableColumn<>("Room Id");
        roomId.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        roomId.setOnEditCommit(event -> event.getRowValue().setRoomId(event.getNewValue()));

        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        age.setCellValueFactory(new PropertyValueFactory<>("age"));
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        roomId.setCellValueFactory(new PropertyValueFactory<>("roomId"));

        fullName.getColumns().addAll(firstName, lastName);

        ObservableList<VIP> list = FXCollections.observableArrayList();
        list.addAll(manageCustumers.getVipCustomers());

        customerTableView.getColumns().addAll(id, fullName, age, email);
        customerTableView.setItems(list);
        return customerTableView;
    }
}
